# OpenML dataset: Diabetes_Dataset

https://www.openml.org/d/46115

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Description:
The "diabetes.csv" dataset is a medical dataset constructed for the evaluation of machine learning models in predicting diabetes occurrences based on various diagnostic measurements. This dataset encapsulates the clinical parameters of several patients, providing a foundational basis for diabetes prediction research and healthcare analytics.

Attribute Description:
1. Pregnancies: Number of times pregnant (Sample Values: 3, 8, 2, 3, 1)
2. Glucose: Plasma glucose concentration a 2 hours in an oral glucose tolerance test (Sample Values: 155, 87, 87, 84, 113)
3. BloodPressure: Diastolic blood pressure (mm Hg) (Sample Values: 68, 0, 70, 80, 80)
4. SkinThickness: Triceps skin fold thickness (mm) (Sample Values: 0, 27, 39, 23, 32)
5. Insulin: 2-Hour serum insulin (mu U/ml) (Sample Values: 105, 0, 110, 325, 194)
6. BMI: Body mass index (weight in kg/(height in m)^2) (Sample Values: 29.7, 38.5, 27.6, 0.0, 27.4)
7. DiabetesPedigreeFunction: Diabetes pedigree function (Sample Values: 0.466, 0.283, 0.252, 0.19, 0.355)
8. Age: Age (years) (Sample Values: 25, 33, 34, 46, 28)
9. Outcome: Class variable (0 or 1) where 1 represents the presence of diabetes and 0 represents absence (Sample Values: 0, 1, 1, 0, 1)

Use Case:
This dataset is immensely useful for researchers, data scientists, and healthcare professionals aiming to develop and validate predictive models for diabetes. It can facilitate a variety of analyses, from basic correlations between variables to advanced machine learning models that predict diabetes occurrence based on patient data. Additionally, the dataset supports educational purposes for students and academicians in medical data analysis and machine learning application fields.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46115) of an [OpenML dataset](https://www.openml.org/d/46115). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46115/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46115/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46115/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

